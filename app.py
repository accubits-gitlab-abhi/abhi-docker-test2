from flask import Flask, render_template, request,jsonify
from flask_mysqldb import MySQL
app = Flask(__name__)

# CREATE USER 'abc'@'localhost' IDENTIFIED BY '987654321';
# create database test_db1;


app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'abc'
app.config['MYSQL_PASSWORD'] = '987654321'
app.config['MYSQL_DB'] = 'test_db1'

mysql = MySQL(app)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        details = request.form
        firstName = details['fname']
        lastName = details['lname']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO MyUsers(firstName, lastName) VALUES (%s, %s)", (firstName, lastName))
        mysql.connection.commit()
        cur.close()
        return '<a href="a">View data from db</a>'
    return render_template('index.html')

@app.route('/a', methods=['GET', 'POST'])
def give():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM MyUsers")
    mysql.connection.commit()
    data = cur.fetchall()
    print("****************************")
    print(data)
    return jsonify(data)
    # cur.close()
    # render_template('template.html', data=data)
    # return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)